/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author DELL
 */
public class InterfaceController implements Initializable {
    conta c = new conta();
    Pessoa p = new Pessoa();
     @FXML
    private TextField txtNome;

    @FXML
    private TextField txtRG;

    @FXML
    private TextField txtCPF;

    @FXML
    private TextField txtEndereco;

    @FXML
    private TextField txtBairro;

    @FXML
    private TextField txtCidade;

    @FXML
    private TextField txtEstado;

    @FXML
    private TextField txtTelefone;

    @FXML
    private TextField txtEmail;

    @FXML
    private TextField txtNumero;

    @FXML
    private TextField txtDigito;

    @FXML
    private TextField txtRetirar;

    @FXML
    private TextField txtAcrescentar;

    @FXML
    private Label Saldo;

    @FXML
    private Label Aviso;

    @FXML
    private void botaoExecutar(ActionEvent event) {
        float valor1, valor2;
        valor1 = c.getSaldo();
        valor2=Float.parseFloat(txtRetirar.getText());
        if(valor1<valor2)
        {
            Aviso.setText( "Não é possivel retirar esse dinheiro");
        }
        else
        {
                    Aviso.setText( "O dinheiro foi retirado");
                    c.setSaldo(valor2);
        }
        valor1 = c.getSaldo();
        Saldo.setText(" " + valor1);
        
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
